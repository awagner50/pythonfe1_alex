"""
Alex Wagner: 11/6/23
Goal: Go back to a previous project and make it better with functions.
I chose the to do list project. All of the functions here are newly added.
The to do list program looks much cleaner and is easier to read.
Also, it is more efficient when done with functions.
I did all 5 extensions for the to do list.
"""


def userInputFunc():
    print('What would you like to do with your list?')
    print('     1) View your list')
    print('     2) Add an item to your list')
    print('     3) Remove an item from your list')
    print('     4) Reverse the order of your list')
    print('     5) Swap the positions of 2 tasks in your list')
    print('     6) Remove multiple tasks from list')  # this is a new extension I added
    print('     7) Clear your list')
    print('     0) Exit the program')
    # try catch is new. Checks for input
    try:
        uInput = int(input())
        return uInput
    except ValueError:
        print('An integer was not inputted')
    print()


def printList():
    print("You have", len(toDoList), "things to do.")
    # for loop prints out the to do list vertically
    c = 1
    for i in toDoList:
        print(str(c) + ':', i)
        c += 1
    print()


def addToList():
    print("Enter a task to add to your list.")
    addInput = input()
    # I got rid of the long for loop and replaced it with if else statement
    # I also added another extension which checks for duplicate tasks in list
    if addInput in toDoList:
        print('Task already in list.')
    else:
        toDoList.append(addInput)
        # for loop checks if addInput was successfully added into the loop
        if addInput in toDoList:
            print('Successful addition to your list!')
        else:
            print('Failure! Something went wrong and your list was not updated.')
    print()


def delFromList():
    print("Enter the index of the task you wish to delete from your list.")
    delInput = int(input())
    del toDoList[delInput - 1]  # makes sure the index matches the index on the output
    # Changed for loop to if else statement to make less lines of code.
    # if else checks if delete worked
    if delInput in toDoList:
        print('Failure! Something went wrong and your list was not updated.')
    else:
        print('Successful deletion from your list!')
    print()


def multiDelFromList():
    # takes 2 inputs as the start and end of the index and deletes the range.
    print("Enter the range of indices you wish to delete.")
    print("Enter start of index range")
    dRangeStart = int(input())
    print("Enter end of index range")
    dRangeEnd = int(input())
    del toDoList[(dRangeStart - 1):dRangeEnd]


def reverseList():
    print('The list will now be reversed.')
    toDoList.reverse()
    print()


def swapList():
    print('Enter the index of the first task you would like swapped.')
    swap1 = int(input()) - 1
    print('Enter the index of the second task you would like swapped.')
    swap2 = int(input()) - 1
    # uses 3 variable. one for the first task, one for the second task, and one as temporary
    temp = toDoList[swap1]  # sets the temp to what the first task was
    toDoList[swap1] = toDoList[swap2]  # replaces the 1st task with the 2nd one
    toDoList[swap2] = temp  # replaces the 2nd tasks old spot with the temp which is the 1st task
    print()


def clearList():
    print('The list will now be cleared.')
    toDoList.clear()
    print()


# Main list to be used throughout the program. Just filled with random things at the beginning.
toDoList = ['Walk the dog', 'Do homework', 'Tend to the garden', 'Write essay', 'Work on project',
            'Clean the bathroom', 'Make dinner', 'Get groceries']
count = 1

print('Welcome to your To Do list!')
# Main loop that keeps running program while user inputs
# This loop looks so much cleaner than previous assignment
# All the code that was here turned into functions that get called when the user inputs the corresponding int
endLoop = True
while endLoop:
    userInput = userInputFunc()
    if userInput == 1:
        printList()
    elif userInput == 2:
        addToList()
    elif userInput == 3:
        delFromList()
    elif userInput == 4:
        reverseList()
    elif userInput == 5:
        swapList()
    elif userInput == 6:
        multiDelFromList()
    elif userInput == 7:
        clearList()
    elif userInput == 0:
        break
    else:
        print('You did not enter a valid number.')
        continue