# Python for Everyone: Alex's student repository

This repository holds code that is used in Python for Everyone taught at Carlow University in Fall 2023

# Project 1: Road trip calculator

This project demonstrates variable names and user input.

# Project 2: Might we be friends

Exploration of the power of nested conditional logic.

# Project 3: Looping Guesses?

This prject demonstrates looping.

# Project 4: Who killed Mr. Boddy?

This prject is centered around strings in Python.

# Project 5: To Do List

This prject uses the power of Lists.

# Project 6: Time Travel

This prject focused on using functions to enhance a previous project

# Project 7: Encryption

This prject demonstrates dictionaries

# Project 8: Reading and Writing files

This prject explores reading and writing files 

# Project 9: Data Visualization

This prject shows off matplotlib and numpy for creating graphs
