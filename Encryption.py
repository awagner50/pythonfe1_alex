"""
Alex Wagner: 11/13/23
Goal: create dictionary which encrypts the entire alphabet and more
"""

# dict of the cipher
cipher = {
    'a': '1',
    'b': 'Z',
    'c': 'Y',
    'd': 'X',
    'e': '2',
    'f': 'V',
    'g': 'U',
    'h': 'T',
    'i': '3',
    'j': 'R',
    'k': 'Q',
    'l': 'P',
    'm': '4',
    'n': 'N',
    'o': 'M',
    'p': 'L',
    'q': '5',
    'r': 'J',
    's': 'I',
    't': 'H',
    'u': '6',
    'v': 'F',
    'w': 'E',
    'x': 'D',
    'y': '7',
    'z': 'B',
    'A': '?',
    'B': 'z',
    'C': '8',
    'D': 'x',
    'E': 'w',
    'F': 'v',
    'G': '9',
    'H': 't',
    'I': 's',
    'J': 'r',
    'K': '0',
    'L': 'p',
    'M': 'o',
    'N': 'n',
    'O': ',',
    'P': 'l',
    'Q': 'k',
    'R': 'j',
    'S': '.',
    'T': 'h',
    'U': 'g',
    'V': 'f',
    'W': ' ',
    'X': 'd',
    'Y': 'c',
    'Z': 'b',
    '1': 'a',
    '2': 'e',
    '3': 'i',
    '4': 'm',
    '5': 'q',
    '6': 'u',
    '7': 'y',
    '8': 'C',
    '9': 'G',
    '0': 'K',
    ',': 'O',
    '.': 'S',
    ' ': 'W',
    '?': 'A'
}

print('I will encrypt any message you want.')
# repeating loop depending on user validation
loop = True
while loop:
    print('Enter the message you want to be encrypted below:')
    userMessage = input()
    messageList = list(userMessage)  # turns the users input into a list
    eMessageList = []  # encrypted message list

    # for loop encrypts the user message by using the cipher
    for c in messageList:
        eMessageList.append(cipher[c])

    print('Your message encrypted is:')
    for i in eMessageList:
        print(i, end='')

    print()
    print('\nWill you encrypt another message?')
    print('Enter any int to continue or 0 to end the program.')
    continueInput = int(input())
    if continueInput == 0:
        print('Goodbye!')
        break
    print()
