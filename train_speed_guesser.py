"""
Creating a program that will guess a value that a user has given within a set range
Alex Wagner: 9/29/23
Licensed under GPL v.3
"""

import random as rand

print("The computer will try to guess the speed you will enter.")
# The train can go max of 400 in either direction
print("Enter the speed of a bullet train in mph(can't go over 400)")
input_speed = float(input())
while -400 >= input_speed or 400 < input_speed:  # Verifying user input is within bounds
    input_speed = float(input())
input_speed = abs(input_speed)  # Makes the speed an even number.
print("How close does the computer need to be to count as correct?")
guess_range = float(input())

guess_count = 0
guess_correct = False
upper_bound = input_speed + guess_range  # calculates the Max speed that can be guessed
lower_bound = input_speed - guess_range  # calculates the Min speed that can be guessed

# loop creates a random int then stops when the rnd int is close enough to the user inputted one
while not guess_correct:
    rnd_guess = rand.uniform(0, 401.00)
    guess_count += 1
    print("Guess", guess_count, ":", rnd_guess, "mph")
    if lower_bound <= rnd_guess <= upper_bound:
        print("-----------------------------------")
        print("Computer guessed within the bounds!")
        print("Its guess is", abs(rnd_guess - input_speed), "mph away from the target of", input_speed)
        print("It took the computer", guess_count, "guesses.")
        guess_correct = True
    else:
        print("...")
print("end loop")
