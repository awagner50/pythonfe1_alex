"""
Alex Wagner: 10/30/23
Goal: Create to do list and allow the user to change it
"""

# Main list to be used throughout the program. Just filled with random things at the beginning.
toDoList = ['Walk the dog', 'Do homework', 'Tend to the garden', 'Write essay', 'Work on project',
            'Clean the bathroom', 'Make dinner', 'Get groceries']

print('Welcome to your To Do list!')
# Main loop that keeps running program while user inputs
endLoop = True
while endLoop:
    print('What would you like to do with your list?')
    print('     1) View your list')
    print('     2) Add an item to your list')
    print('     3) Remove an item from your list')
    print('     4) Reverse the order of your list')
    print('     5) Swap the positions of 2 tasks in your list')
    print('     6) Clear your list')
    print('     0) Exit the program')
    userInput = int(input())

    # count is to increment the to do list
    count = 1
    # If user wants to view loop
    if userInput == 1:
        print("You have", len(toDoList), "things to do.")
        # for loop prints out the to do list vertically
        for i in toDoList:
            print(str(count) + ':', i)
            count += 1
        print()
    # if the user wants to add to list
    elif userInput == 2:
        print("Enter a task to add to your list.")
        addInput = input()
        toDoList.append(addInput)
        # for loop checks if addInput was successfully added into the loop
        inputAddChecker = False
        for i in toDoList:
            if i == addInput:
                inputAddChecker = True
                break
            else:
                inputAddChecker = False
        if inputAddChecker:
            print('Successful addition to your list!')
        else:
            print('Failure! Something went wrong and your list was not updated.')
        print()
    # if the user wants to delete from list
    elif userInput == 3:
        print("Enter the index of the task you wish to delete from your list.")
        delInput = int(input())
        del toDoList[delInput - 1]  # makes sure the index matches the index on the output
        # for loop checks if delInput was successfully deleted from the loop
        inputDelChecker = False
        for i in toDoList:
            if i == delInput:
                inputDelChecker = True
                break
            else:
                inputDelChecker = False
        if not inputDelChecker:
            print('Successful deletion from your list!')
        else:
            print('Failure! Something went wrong and your list was not updated.')
        print()
    # if the user wants to reverse the list
    elif userInput == 4:
        print('The list will now be reversed.')
        toDoList.reverse()
        print()
    # if the user wants to swap to tasks within the list
    elif userInput == 5:
        print('Enter the index of the first task you would like swapped.')
        swap1 = int(input()) - 1
        print('Enter the index of the second task you would like swapped.')
        swap2 = int(input()) - 1
        # uses 3 variable. one for the first task, one for the second task, and one as temporary
        temp = toDoList[swap1]  # sets the temp to what the first task was
        toDoList[swap1] = toDoList[swap2]  # replaces the 1st task with the 2nd one
        toDoList[swap2] = temp  # replaces the 2nd tasks old spot with the temp which is the 1st task
        print()
    # if the user wants to clear/wipe the list
    elif userInput == 6:
        print('The list will now be cleared.')
        toDoList.clear()
        print()
    # if the user wants to stop
    elif userInput == 0:
        print('Thank you for using your To Do List.')
        break
        # if the user enters a number that is not an option
    else:
        print('You did not enter a valid number.')
        continue
