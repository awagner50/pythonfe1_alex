"""
Alex Wagner: 12/13/23
Goal: create a series of 5 visualizations based on the British Film Institute's (BFI) yearly released data
"""
import matplotlib.pyplot as plt
import numpy as np

# Pie Chart all releases in the UK and Republic of Ireland by BBFC film classification, 2020
pieLabels = np.array(['U', 'PG', '12A', '15', '18', 'No classification'])
pieData = np.array([30, 50, 137, 186, 36, 3])
pieExplode = [0, 0, 0, 0.05, 0, 0]
pieFont1 = {'family': 'serif', 'color': 'dimgray', 'size': 20}  # for pie chart title
pieFont2 = {'family': 'serif', 'color': 'dimgray', 'size': 12}  # for the pie chart %'s
pieColors = ['lightcoral', 'wheat', 'palegreen', 'plum', 'lightskyblue', 'lightpink']

plt.pie(pieData, colors=pieColors, explode=pieExplode, autopct='%1.1f%%', pctdistance=1.15, textprops=pieFont2)
plt.legend(title='BBFC film classification', labels=pieLabels, loc='center left', bbox_to_anchor=(1, 0, 0.5, 1))
plt.title(label='Figure 1: All Releases in the UK and Republic of Ireland\n '
                'by BBFC Film Classification, 2020', fontdict=pieFont1)
plt.show()

# Line Graph of Annual UK cinema admissions, 2012-2021
lineX = np.array(['2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019', '2020', '2021'])
lineY = np.array([172.5, 165.5, 157.5, 171.9, 168.3, 170.6, 177, 176.1, 44, 74])
lineFont1 = {'family': 'monospace', 'color': 'black', 'size': 24}  # for line chart title
lineFont2 = {'family': 'monospace', 'color': 'black', 'size': 12}  # for the line chart info

plt.plot(lineX, lineY, marker='o', ms=5.5, mec='k', mfc='w', ls='-', lw=2.5, c='crimson')
plt.xlabel('Years', fontdict=lineFont2)
plt.ylabel('Admissions (million)', fontdict=lineFont2)
plt.title('Figure 2: Annual UK cinema admissions, 2012-2021', fontdict=lineFont1)
plt.show()

# Scatterplot of cinema admissions by ISBA TV region, 2021
splotX = np.array([13.7, 9.1, 7.7, 7.2, 6.5, 5, 3.9, 3.1, 2.8, 2.5, 1.9, 1.8, 1.3, 0.6])
splotY = np.array([16.9, 8.9, 8.5, 8.3, 6.6, 5.6, 4.7, 3, 2.9, 2.5, 2.5, 1.9, 1.3, 0.4])
splotFont1 = {'family': 'serif', 'weight': 'bold', 'color': 'cadetblue', 'size': 18}  # for scatterplot chart title
splotFont2 = {'family': 'serif', 'color': 'black', 'size': 12}  # for the scatterplot chart info

plt.scatter(splotX, splotY, marker='D', s=25, c='cadetblue')
plt.xlabel('Population (million)', fontdict=splotFont2)
plt.ylabel('Admissions (million)', fontdict=splotFont2)
plt.title('Figure 3: Cinema Admissions vs. Populations by ISBA TV region, 2021', fontdict=splotFont1)
plt.show()

# Bar graph of films released in the UK and Republic of Ireland by genre, 2021
bgraphX = np.array(['Drama', 'Documentary',  'Comedy', 'Action', 'Animation', 'Horror', 'Romance', 'Suspense',
                    'Thriller', 'Family', 'Musical', 'Adventure', 'Biopic', 'Sci fi', 'Fantasy', 'Crime'])
bgraphY = np.array([134, 69, 54, 52, 38, 24, 13, 12, 10, 8, 8, 6, 5, 4, 3, 2])
bgraphFont1 = {'family': 'fantasy', 'color': 'black', 'size': 28}  # for bar graph title
bgraphFont2 = {'family': 'serif', 'color': 'black', 'size': 14}  # for the bar graph info

plt.xlabel('Genre', fontdict=bgraphFont2)
plt.ylabel('Number of releases', fontdict=bgraphFont2)
plt.title('Figure 4: Films released in the UK and Republic of Ireland by genre, 2021', fontdict=bgraphFont1)
plt.bar(bgraphX, bgraphY, width=.75, color='darkorange')
plt.show()

# Histogram of Annual UK admissions, 1935-2021
histogramX = np.array([176, 156.6, 134.2, 138.5, 116.3, 103.9, 103.5, 126.1, 111.9, 101, 86, 64, 65.7, 54, 72, 75.5,
                       78.5, 84, 94.5, 97.4, 100.3, 103.6, 114.4, 123.5, 114.6, 123.5, 138.9, 135.2, 139.1, 142.5,
                       155.9, 175.9, 167.3, 171.3, 164.7, 156.6, 162.4, 164.2, 173.5, 169.2, 171.6, 172.5, 165.5,
                       157.5, 171.9, 168.3, 170.6, 177, 176.1, 44, 74])
histoFont1 = {'family': 'sans-serif', 'color': 'black', 'size': 20}  # for histogram title
histoFont2 = {'family': 'sans-serif', 'color': 'black', 'size': 13}  # for histogram info

plt.xlabel('Admissions (million)', fontdict=histoFont2)
plt.ylabel('frequency', fontdict=histoFont2)
plt.title('Figure 5: Annual UK Admissions, 1971-2021', fontdict=histoFont1)
plt.hist(histogramX, bins=10, range=(40, 180), edgecolor='black', linewidth=1.2, color='lightseagreen')
plt.show()
