"""
Alex Wagner: 9/20/23
Goal: make a quiz that tests if we could be friends
 by using conditionals
"""

# variable to keep track of the total user compatibility score
score = 0

print("Maybe we can become friends?")
print("My name is Alex. What is your Name?")
name = input()
print("Hello", name + ", nice to meet you!")
print("Lets take a friend compatibility test")
print("On all questions enter 0 for No or 1 for Yes")

# Question 1 and a nested question 2
print("\nDo you like hockey?")
q1_hockey = int(input())
if q1_hockey == 1:
    score += 5
    print("Nice, I enjoy Hockey too!")
    print("Are you a fan of the Pens?")
    q2_pens = int(input())
    if q2_pens == 1:
        score += 5
        print("I'm intrigued to see how they do this year.")
    else:
        score -= 15
        print("hmmm, as long as you aren't a fan of the flyers, caps, or rangers.")

else:
    score -= 1
    print("That's okay, it's not for everyone.")

# Question 3
print("\nDo you watch movies, TV series, or anime?")
q3_movies = int(input())
if q3_movies == 1:
    score += 15
    print("Cool, that will give us a ton to talk about.")
else:
    score -= 25
    print("Really, you don't watch anything?")

# Question 4
print("\nOn a scale from 1 - 10: How much do you enjoy video games?")
q4_games = int(input())
if q4_games == 10:
    score += 30
    print("That's great to hear.")
elif 6 <= q4_games <= 9:
    score += 25
    print("Nice, I love playing games in my free time.")
elif 4 <= q4_games <= 5:
    score += 5
    print("Okay, I bet I could find a game we'll both enjoy")
else:
    score -= 25
    print("It's your loss")

# Question 5 and a nested question 6
print("\nDo you like listening to music?")
q5_music = int(input())
if q5_music == 1:
    score += 10
    print("Music is great for any mood.")
    print("Do you like any of these genres: Metal, Pop, Rock, or Rap")
    q6_genre = int(input())
    if q6_genre == 1:
        score += 5
        print("Maybe we share some favorite artists or bands.")
    else:
        score -= 5
        print("All hope is not lost, I like other genres too.")

else:
    score -= 10
    print("Well, you do you then.")

# Question 7
print("\nDo you have any pets?")
q7_animals = int(input())
if q7_animals == 1:
    score += 5
    print("Me too, we can share photos.")
else:
    score -= 1
    print("That's okay, everybody's situation is different.")

print("\n-------------------- END TEST --------------------")
MAX_PTS = 75  # the highest number of points that can be scored, not changed
compat_prct = (score / MAX_PTS) * 100.
print("\nYou scored:", score, "out of", MAX_PTS)
print("Which is {:03.2f}% compatibility".format(compat_prct))
# print("Your scaled compatibility is {:03.2f} percent".format(perc_possible))
# if statement to determine compatibility
if compat_prct == 100:
    print("Wow, you got a perfect score. Are we the same person?")
elif compat_prct >= 85:
    print("I can see us getting along just fine.")
elif compat_prct >= 70:
    print("We have quite a few things in common.")
    print("There is a good chance for friendship.")
elif compat_prct >= 50:
    print("Not too shabby, we have a few things in common.")
    print("There's a slight chance for friendship")
elif compat_prct >= 0:
    print("A friendship probably won't work out.")
    print("We can still be acquaintances.")
else:
    print("How did you even score this low? This will never work out.")
