"""
Alex Wagner: 11/20/23
Goal: write a program that will create a FASTA file for the 3 given sequences
"""


# function takes in the fasta header and dna sequence, then makes them into their own file
def fasta_maker(header, dna):
    fastaFile = open(header + '.fasta', 'w')
    fastaFile.write('>' + header.lower() + '\n')
    fastaFile.write(dna.upper() + '\n')
    fastaFile.close()


# ingredients to make the fasta file soup
header1 = 'ABC123'
header2 = 'DEF456'
header3 = 'HIJ789'
dna1 = 'ATCGTACGATCGATCGATCGCTAGACGTATCG'
dna2 = 'actgatcgacgatcgatcgatcacgact'
dna3 = 'ACTGACtACTGTctACTGTAccttCATGTG'

# creates a FASTA file with 3 dna sequences in fasta format
FASTA_sequences = open('FASTA_sequences.fasta', 'w')
FASTA_sequences.write('>' + header1.lower() + '\n')
FASTA_sequences.write(dna1.upper() + '\n')
FASTA_sequences.write('>' + header2.lower() + '\n')
FASTA_sequences.write(dna2.upper() + '\n')
FASTA_sequences.write('>' + header3.lower() + '\n')
FASTA_sequences.write(dna3.upper() + '\n')
FASTA_sequences.close()

# Extension 1: calls function to make sequence into its own fasta file
# Puts in the fasta header and dna sequence
fasta_maker(header1, dna1)
fasta_maker(header2, dna2)
fasta_maker(header3, dna3)

# Extension 2: remove all the dashes from the given dna sequence then make it into a fasta file
headerX = 'XYZ321'
dnaX = 'ACTGAC---ACTGTA-CTGTA----CATGTG'
given_FASTA_sequence = open('XYZ321.fasta', 'w')
given_FASTA_sequence.write('>' + headerX.lower() + '\n')
given_FASTA_sequence.write(dnaX.replace('-', '') + '\n')
given_FASTA_sequence.close()
