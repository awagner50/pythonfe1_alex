"""
Alex Wagner: 10/23/23
Goal: Determine who killed Mr. Boddy using Pyton strings
"""

print('Mr. Boddy was killed in the study with the candlestick.')
print('But the question is by whom?')
print('There were two short DNA samples found on the murder weapon.')
print('Using the the invited guests DNA we will find out who done it.\n')

# DNA of the 6 murder suspects as strings
scarletDNA = 'CCTGGAGGGTGGCCCCACCGGCCGAGACAGCGAGCATATTCAGGAAGCGGCAGGAATAAGGAAAAGCAGC'
peacockDNA = 'AAGCTCGGGAGGTGGCCAGGCGGCAGGAAGGCGCACCCCCCCAGTACTCCGCGCGCCGGGACAGAATGCC'
whiteDNA = 'ATATCCCGTACGTTAGAAGCATAATAGATATAGGGGETATAGAGGATACATCGATCGATTTATTTTTACCCCGAT'
plumDNA = 'CTGCAGGAACTTCTTCTGGAAGTACTTCTCCTCCTGCAAATAAAACCTCACCCATGAATGCTCACGCAAG'
mustardDNA = 'CTCCTGATGCTCCTCGCTTGGTGGTTTGAGTGGACCTCCCAGGCCAGTGCCGGGCCCCTCATAGGAGAGG'
greenDNA = 'TTTGATCTCTAGCTAGCTCGCGCTCATAGCTCGCTATATAGCGCGCTAGCTGACTATCGATCGATCGATCGCTA'

# list of all the DNA sequences to be used in for loop
dnaList = [scarletDNA, peacockDNA, whiteDNA, plumDNA, mustardDNA, greenDNA]
# list of all the suspects to be used in for loop
suspectList = ['Miss Scarlet', 'Mrs. Peacock', 'Mrs. White', "Prof Plum", 'Colonel Mustard', 'Mr. Green']

# DNA found on the murder weapon
eDNA1 = 'CATA'
eDNA2 = 'ATGC'
x = 0
murderer = ''

print("*** NOW COMPARING DATA ***")
# for loop which loops through the entire dnaList.
# It then determines if both murder weapon dna sequences are found in any of the suspects dna
for i in dnaList:
    if eDNA1 in i and eDNA2 in i:
        print(suspectList[x], ': Match', sep='')
        murderer = suspectList[x]
        x += 1
    else:
        print(suspectList[x], ": Mismatch", sep='')
        x += 1

print("\nAHA a match... the one who murdered Mr. Boddy is...", murderer)
print('DUN DUN DUNNNNN!!!')
print('---------- END PART 1 ----------')

# Extensions START
n = 0
print('What is the DNA of the suspect?')
inputDNA = input()
inputDNA = inputDNA.upper()
print('What is the first DNA sample that belongs to the killer')
inputKillerDNA1 = input()
inputKillerDNA1 = inputKillerDNA1.upper()
print('What is the second DNA sample that belongs to the killer')
inputKillerDNA2 = input()
inputKillerDNA2 = inputKillerDNA2.upper()
print('\n')

# for loop that determines if the inputted killer DNA samples is a match or not to the suspects inputted dna
x = 0
foundKiller = False
for i in dnaList:
    if inputDNA == i:
        if inputDNA.find(inputKillerDNA1) != -1 and inputDNA.find(inputKillerDNA2) != -1:
            print(suspectList[x], ': Match', sep='')
            murderer = suspectList[x]
            x += 1
            foundKiller = True
        else:
            print(suspectList[x], ": Mismatch", sep='')
            x += 1
            foundKiller = False
    else:
        x += 1

idx1 = inputDNA.find(inputKillerDNA1)
idx2 = inputDNA.find(inputKillerDNA2)
print('DNA match at:', idx1, 'and', idx2)

if foundKiller:
    print("The one who murdered Mr. Boddy is", murderer)
