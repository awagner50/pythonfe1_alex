"""
Python for Everyone 1: Week 2 - Variables
Alex Wagner
Goal: complete and compare mileage and
fuel use across small and large cars
"""

print("Welcome to road trip efficiently comparisons!")

small_vehicle = "2015 smart fortwo coupe"
large_vehicle = "2010 Hummer H3 4WD"

small_vehicle_mpg = 36.0
large_vehicle_mpg = 16.0

small_vehicle_url = "https://www.fueleconomy.gov/feg/photos/2013_smart_fortwo_coupe.jpg"
large_vehicle_url = "https://www.fueleconomy.gov/feg/photos/2010_Hummer_H3.jpg"

print("The", small_vehicle, "can drive", small_vehicle_mpg,
      "miles per gallon of gasoline")
print("info can be found at", small_vehicle_url)
print("The", large_vehicle, "can drive", large_vehicle_mpg,
      "miles per gallon of gasoline")
print("info can be found at", large_vehicle_url)

print("-------------------------------------------")
print("Road trip! How many miles? (decimal places)")
trip_miles = float(input())

# gallons computed by diving trip length by miles per gallon
small_vehicle_gallons = trip_miles / small_vehicle_mpg
large_vehicle_gallons = trip_miles / large_vehicle_mpg
gallons_diff = large_vehicle_gallons - small_vehicle_gallons

print("The", small_vehicle, "will require", small_vehicle_gallons,
      "gallons of fuel to travel", trip_miles, "miles.")
print("The", large_vehicle, "will require", large_vehicle_gallons,
      "gallons of fuel to travel", trip_miles, "miles.")
print("The", large_vehicle, "will need", gallons_diff, "more gallons than the",
      small_vehicle)

print("-------------------------------------------")
print("What's the expected average cost per gallon of gas on your trip?")
# average price per gallon of gasoline
avg_ppg_gas = float(input())

# Total trip cost calculated by multiplying the gallons of fuel required by the price per gallon
small_vehicle_total = small_vehicle_gallons * avg_ppg_gas
large_vehicle_total = large_vehicle_gallons * avg_ppg_gas
total_diff = large_vehicle_total - small_vehicle_total

print("The total expected cost of", small_vehicle, "is", small_vehicle_total)
print("The total expected cost of", large_vehicle, "is", large_vehicle_total)
print("The cost difference between the two trips is:", large_vehicle_total,
      '-', small_vehicle_total, "=", total_diff)

highway_speed = 70
road_speed = 35

print()
print("How much over the posted speed limit do you drive on highways?")
highway_speed_over = int(input())
print("How much over the posted speed limit do you drive on local roads?")
road_speed_over = int(input())

highway_actual_speed = highway_speed + highway_speed_over
road_actual_speed = road_speed + road_speed_over
# Expected travel time calculation is total trip miles divided by travel rates in mph
highway_travel_time = trip_miles / highway_actual_speed
road_travel_time = trip_miles / road_actual_speed

print("The", small_vehicle, "travel time will be", highway_travel_time, "hours on the highway or",
      road_travel_time, "hours on local roads")
print("The", large_vehicle, "travel time will be", highway_travel_time, "hours on the highway or",
      road_travel_time, "hours on local roads")
